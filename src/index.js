import React from 'react'
import ReactDOM from 'react-dom'
import Page from './components/page'


ReactDOM.render(
    React.createElement(Page, null), 
    document.getElementById('root')
)